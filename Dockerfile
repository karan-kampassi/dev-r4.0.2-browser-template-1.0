FROM r-base:4.0.2

ARG PKGS="shiny, Hmisc, rjson, caret, DBI, RPostgres,aws.s3"

RUN apt-get update \
 && apt-get install -y --auto-remove \
    build-essential \
    libcurl4-openssl-dev \
    libpq-dev \
    libssl-dev \
    libxml2-dev

RUN Rscript -e 'install.packages("pak", repos = "https://r-lib.github.io/p/pak/dev/")' \
 && echo "$PKGS" \
  | Rscript -e 'pak::pkg_install(strsplit(readLines("stdin"), ", ?")[[1L]])'

RUN mkdir /shinyapp
COPY . /shinyapp

EXPOSE 5000
CMD ["Rscript", "-e", "shiny::runApp('/shinyapp/src/shiny', port = 5000, host = '0.0.0.0')"]